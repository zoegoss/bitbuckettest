from opentidalfarm import *
import numpy as np
import argparse
set_log_level(INFO)

# build the domain
domain = FileDomain('EC_mesh_UTM30.xml')
prob_params = SteadySWProblem.default_parameters()
prob_params.domain = domain
V = FunctionSpace(domain.mesh, 'CG', 1)

# Load the bathymetry
utm_zone = 30
utm_band = 'U'
#bathy_expr = BathymetryDepthExpression('bathymetry.nc',
#         utm_zone=utm_zone, utm_band=utm_band, domain=domain.mesh)
#prob_params.depth = bathy_expr
prob_params.depth = 50 # bathy_expr

# set the boundary conditions
bcs = BoundaryConditionSet()
bcs.add_bc("u", Constant((0.8, 0.08)), facet_id=1)
#bcs.add_bc("eta", Constant(1.), facet_id=1)
bcs.add_bc("eta", Constant(0), facet_id=2)
bcs.add_bc("u", facet_id=3, bctype="free_slip")

# set physical parameters
prob_params.bcs = bcs
prob_params.viscosity = Constant(1000)
prob_params.friction = Constant(0.0025)
prob_params.initial_condition = Expression(("1e-7", "0", "0"))

# create the farm and the domains
W = FunctionSpace(domain.mesh, "DG", 0)
farm = Farm(domain, SmearedTurbine(), function_space=W)
domains = MeshFunction("size_t", domain.mesh, "EC_mesh_UTM30_physical_region.xml")
dx = Measure("dx")[domains]
farm.site_dx = dx((1,2))
prob_params.tidal_farm = farm

# set up problem
problem = SteadySWProblem(prob_params)
sol_params = CoupledSWSolver.default_parameters()
sol_params.dump_period = 1
sol_params.cache_forward_state = False
solver = CoupledSWSolver(problem, sol_params)

#Print outputs
pf = PowerFunctional(problem)
cf = CostFunctional(problem)

text_file_P_ATW = open("Power_ATW.txt", "w")
text_file_P_FTW = open("Power_FTW.txt", "w")
text_file_C_ATW = open("Cost_ATW.txt", "w")
text_file_C_FTW = open("Cost_FTW.txt", "w")
text_file_n_ATW = open("n_ATW.txt", "w")
text_file_n_FTW = open("n_FTW.txt", "w")

#cost_farm1 = farm.friction_function * dx(1)
#cost_farm2 = farm.friction_function * dx(2)
#power_farm1 = pf.power(solver.state, farm.friction_function) * dx(1)

class MyReducedFunctional(ReducedFunctional):
  def _dj(self, m, forget, new_optimisation_iteration=True):
     
     power = pf.Jt(solver.state, farm.friction_function)
     power_farm1 = pf.power(solver.state, farm.friction_function) * dx(1)
     power_farm2 = pf.power(solver.state, farm.friction_function) * dx(2)

     print "POWER_ATW", assemble(power_farm1)
     print "POWER_FTW", assemble(power_farm2)
     text_file_P_ATW.write(str(assemble(power_farm1)) + '\n')
     text_file_P_FTW.write(str(assemble(power_farm2)) + '\n')

     #cost = cf.Jt(solver.state, farm.friction_function)
     cost_farm1 = farm.friction_function * dx(1)
     cost_farm2 = farm.friction_function * dx(2)
     text_file_C_ATW.write(str(assemble(cost_farm1)) + '\n')
     text_file_C_FTW.write(str(assemble(cost_farm2)) + '\n')
     
     n_ATW=(2*(assemble(cost_farm1)))/(0.8*201)
     n_FTW=(2*(assemble(cost_farm2)))/(0.8*201)
     text_file_n_ATW.write(str(n_ATW) + '\n')
     text_file_n_FTW.write(str(n_FTW) + '\n')
     
     return super(MyReducedFunctional, self)._dj(m, forget, new_optimisation_iteration=new_optimisation_iteration)

# build functional
functional = PowerFunctional(problem)-1600*CostFunctional(problem)

# build reduced functional
control = TurbineFarmControl(farm)
rf_params = MyReducedFunctional.default_parameters()
rf_params.automatic_scaling = None
rf = MyReducedFunctional(functional, control, solver, rf_params)

# optimise
f_opt = maximize(rf, bounds=[0, 0.05],
                 method="L-BFGS-B", options={'maxiter': 15})
